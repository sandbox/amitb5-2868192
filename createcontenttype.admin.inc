<?php

/**
 * @file
 * Form function.
 */

 /**
  * Set values for content type form.
  */
function createcontenttype_form($form, &$form_state) {
  $form['#attributes'] = array(
    'enctype' => 'multipart/form-data',
  );
  /* the options to display checkboxes */
  $options = array(
    'news' => t('News'),
  );
  // The drupal checkboxes form field definition.
  $form['contenttypes'] = array(
    '#title' => '',
    '#type' => 'checkboxes',
    '#options' => $options,
    '#default_value' => variable_get('createcontenttype_contenttypes'),
    '#prefix' => "<strong>This form will create below selected content type and its block.</strong>",
  );
  $form['submit'] = array(
    '#value' => t('Submit'),
    '#type' => 'submit',
  );
  return $form;
}

/**
 * Create selected content type.
 */
function createcontenttype_form_submit($form, &$form_state) {
  variable_set('createcontenttype_contenttypes', $form_state['values']['contenttypes']);
  createcontenttype_create($form_state['values']);
}
